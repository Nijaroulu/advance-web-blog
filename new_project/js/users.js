//Get All events, update event, delete event
var app = new Vue({
  el: '#appEvents',
  data: {
    info:null,
    url:'http://127.0.0.1:8000/users/',
    nickname:null,
    email:null,
    full_name:null,
    message:null,
    updateUrl:null,
    show:false,
  },
  mounted () {
      //Get User ALl users
      axios
      .get('http://127.0.0.1:8000/users/?format=json')
      .then(response => (this.info = response));
  },

  methods: {
      //delete a user
      de: function (id) {

      axios
      .delete(this.url+id+'/');
      setTimeout(function(){ location.reload(); }, 50);
      },
      update: function (id,nick,fullName,e_mail) {

      this.updateUrl=this.url+id+'/';
      this.nickname=nick;
      this.show=true;
      this.email=e_mail;
      this.full_name=fullName;
      },

      add: function () {

      axios
        //update user
        .put(this.updateUrl, {
          "nickname": this.nickname,
          "email": this.email,
          "full_name":this.full_name,})
        .then(response => (this.message = response.statusText));
        setTimeout(function(){ location.reload(); }, 1050);
      },
    }
  
})




