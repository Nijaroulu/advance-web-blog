from django.contrib.auth.models import User
from rest_framework import serializers
from quickstart.models import TUser

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = TUser
        fields = [
                'id',
                'nickname',
                'email',
                'full_name',
            ]

