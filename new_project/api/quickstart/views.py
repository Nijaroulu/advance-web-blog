from django.contrib.auth.models import User
from rest_framework import viewsets
from quickstart.serializers import UserSerializer
from quickstart.models import TUser


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = TUser.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
