# PWP SPRING 2019
# PROJECT NAME:
Blog administration
# Group information
* Student 1. Nijar Hossain, nhossain18@student.oulu.fi,nijar2011@gmail.com
* Student 2. Nasrin Akter, nakter18@student.oulu.fi


# Client Descriptions
We have developed the client based on HTML and CSS.Within the poject repository a folder called templates (index.html - for the home page and add_member.html is reponsible for adding user to the system.)

# API Resources Implemented in Client
Total  resources and 2 API request implemented in the client.<br>
1.api/users/ (GET, POST)
2.api/users/id/(GET,UPDATE,DELETE)


#API part
1.At first user need to create virtualenv.
1.Secondly, go to the api folder and execute the requirements.txt file for the dependencies with the command "pip install -r requirements.txt".
3.Just follow the instrucitons below:

F:\>cd mypython

F:\mypython>cd api

F:\mypython\api>python manage.py runserver
Performing system checks...

System check identified no issues (0 silenced).
June 10, 2019 - 10:31:54
Django version 2.1.4, using settings 'tutorial.settings'
Starting development server at http://127.0.0.1:8000/
Quit the server with CTRL-BREAK.


# For running the client part
1. First run the api in http://127.0.0.1:8000/ in django console in api folder<br>
2. After that go to the repository's templates folder > run the index.html file<br>
3.HTML file will automatically fetch the data by using the rest API <br>

# Client Repository Description
1. Static- contains css, images, some javascript libraries 

2. Templates: We have designed two html file for the following actions.<br>
	-index.html showing all the list of the users and client can delete and update the user by their choice <br>	
	-add_member.html for creating new user<br>

3. JS- We have developed two javascript file for the following actions.<br>
	-users.js performing the action of getting all users by GET request, Update a user,Delete a user by DELETE request<br>
	-AddUser.js performing action of cretaing new users by sending POST request<br>	
	
